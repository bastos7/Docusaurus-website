# TP : A la rescousse de mon pipeline !

## Contexte
Le répertoire courant permet de construire un site Web basé sur le framework Docusaurus (générateur de site statique open source conçu principalement pour la documentation de projets) et de le déployer dans Gitlab Pages qui le service d'hébergement intégré dans GitLab permettant le déploiement simple et automatisé de sites web statiques directement depuis des dépôts GitLab.

Afin d'automatiser le déploiement de ce site sur Gitlab-Pages, un pipeline Gitlab-CI a été crée et s'exécute dès qu' une modification (commit) est détecté.

Celui-ci permet d'automatiser la construction, les tests et le déploiement du site web statique à l'aide de GitLab CI. Il utilise Node.js pour installer les dépendances, construire le projet, effectuer des vérifications de qualité (linting) sur les fichiers Markdown et HTML, puis déploie le site sur GitLab Pages.

## Problème

Celui-ci semble avoir été sévèrement altéré par le stagiaire et ne fonctionne plus et, pour une raison inconnu, nous ne pouvons pas revenir sur les commits précédents. 

BREF, Il va falloir le réparer à la main et vérifier que tout fonctionne ! 
Par chance, vous retrouvez dans un mail un ensemble d'instructions qui va pour permettre de le réparer.
 
## Instructions 

Comme vu précédemment, l'outil Git seul, ne sait pas faire de pipeline. Cette fonctionnalité n'est possible que via certains outils comme Gitlab ou Jenkins. Nous utiliserons Gitlab dans notre cas.

Faire un "fork" du projet Gitlab dans son propre espace Gitlab.com puis travailler dans la branche principale "main"

1.  Commence d'abord par recréer les  (stages) clés du pipeline, tous les jobs doivent faire parti d'un stage pour être joué.
        
2.  Créer le job de build (`build-job`) :
    
    -   Définir le stage comme `build`.
    -   Utiliser l'image Docker `node`.
    -   Ajouter les étapes de script :
        -   Installer les dépendances avec `npm install`.
        -   Lancer la construction avec `npm run build`.
    -   Définir les artefacts à sauvegarder dans le répertoire `build/`.

3.  Créer le job de linting Markdown (`lint-markdown`) :
    
    -   Le stage est le même que le job suivant.
    -   Utiliser la même image Docker que le job précédent.
    -   Spécifier aucune dépendance pour éviter de récupérer des artefacts.
    -   Ajouter les étapes de script :
        -   Installer `markdownlint-cli2` globalement avec `npm install markdownlint-cli2 --global`.
        -   Lancer le linting sur les fichiers Markdown dans les répertoires `blog/` et `docs/`.
    - Autoriser le stage à échouer.

4.  Créer le job de test HTML (`test-html`) :
    
    -   Définir le stage comme `test`.
    -   Utiliser la même image Docker que le job précédent.
    -   Définir la dépendance uniquement sur le job de build (`build-job`).
    -   Ajouter les étapes de script :
        -   Installer `htmlhint` en tant que dépendance de développement avec `npm install --save-dev htmlhint`.
        -   Lancer le linting HTML sur les fichiers dans le répertoire `build/`.

5.  Créer le job de déploiement (`pages`) :
    
    -   Définir le stage comme `deploy`.
    -   Définir la dépendance uniquement sur le job de build (`build-job`).
    -   Ajouter les étapes de script :
        -   Déplacer les artefacts du répertoire `build/` vers `public/`.
    -   Définir les artefacts à sauvegarder dans le répertoire `public/`.